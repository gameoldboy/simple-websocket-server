//传端口参数
var arguments = process.argv;
var port = 8080;
if(arguments[2] !== undefined){
	port = arguments[2];
}

const WebSocket = require('ws');

const wss = new WebSocket.Server({
	port : port //端口号
});
//player唯一id（pid）
var pid = 0;

console.log('The server is ready, enjoy!\nAuthor:Freezer.CH\nVersion: '+
0.02
+'\nPort: '+
port
+'\nPress CTRL + C to terminate the server.\n');

// Broadcast to all.
wss.broadcast = function broadcast(data) {
  wss.clients.forEach(function each(client) {
	if (client.readyState === WebSocket.OPEN) {
	  client.send(data);
	}
  });
};

wss.on('connection', function connection(ws) {
	
	//分配给登陆者唯一id
	ws.pid = ++pid;
	//默认匹配起一个名字
	ws.nickName = "player"+ws.pid;
	player = new Object();
	player.event = 'login';
	allPlayer = new Array();
	
	//遍历推送登陆事件
	console.log('=>current online players:');
	wss.clients.forEach(function each(client) {
		if(client.readyState === WebSocket.OPEN){
			player.pid = client.pid; //当前用户pid
			player.lastPID = ws.pid; //最后登陆的pid
			client.send(JSON.stringify(player));
			console.log('  └Nick Name: \"'+client.nickName+'\" pid: '+client.pid);
		}
	});
	
	//推送数据（自定义）
	ws.on('message', function incoming(data) {
		
		msgJSON = JSON.parse(data);
		//客户端事件
		switch(msgJSON.event){
			
			//获得所有在线客户端
			case "getAllPlayer":
				player = new Object();
				player.event = 'returnAllPlayer';
				player.allPlayer = new Array();
				wss.clients.forEach(function each(client) {
					if(client.readyState === WebSocket.OPEN){
						player.allPlayer.push({pid:client.pid,nn:client.nickName})
					}
				});
				ws.send(JSON.stringify(player));
				//console.log(player.event);
				break;
				
			//设置昵称，并推送给其他人
			case "setNickName":
				ws.nickName = msgJSON.nickName;
				player = new Object();
				player.event = 'playerChangeName';
				player.pid = ws.pid;
				player.nickName = ws.nickName;
				wss.clients.forEach(function each(client) {
					if(client.readyState === WebSocket.OPEN){
						client.send(JSON.stringify(player));
					}
				});
				console.log('=>player change name to \"'+player.nickName+'\" form pid: '+player.pid);
				break;
		}
		
		//数据推给所有用户
		wss.clients.forEach(function each(client) {
			// if (client !== ws && client.readyState === WebSocket.OPEN) {
				// client.send(data);
			// }
			if (client.readyState === WebSocket.OPEN) {
				client.send(data);
			}
		});
		//console.log(data);
	});
	
	//ws协议关闭
	ws.on('close', function () {
		
		//广播玩家离开
		wss.clients.forEach(function each(client) {
			if(client.readyState === WebSocket.OPEN){
				player = new Object();
				player.event = 'closed';
				player.pid = ws.pid;
				client.send(JSON.stringify(player));
			}
		});
		console.log("<=closed pid: "+ws.pid);
	});
});