# 简易的Websocket服务器 #

* 适用于快速原型，可在windows、linux服务器上运行，服务器没有逻辑（仅广播）
* 基于json格式传输

* 需要先安装node.js https://nodejs.org/en/
* windows上直接运行相应的bat批处理程序即可完成安装Websocket、运行服务器

* 打开C2填入websocket服务器地址和对应的端口号，windows上本机ip可以在ipconfig中查看
* 注意：公网填公网ip，局域网填本地ip


### 服务端事件event ###

1. login 玩家登陆
	* pid 玩家id
	* last pid 最后登录id
2. closed 玩家离开
	* pid 玩家id
3. returnAllPlayer 返回所有玩家数组
	* all player array 所有玩家数组
4. playerChangeName 有玩家更名
	* pid 玩家id
	* nick name 玩家昵称

### 客户端事件event ###

1. getAllPlayer 请求获得所有玩家数组
2. setNickName 更名
	* nick name 玩家昵称